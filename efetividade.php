<?php

set_time_limit(3600);
header('Content-Type: text/html; charset=UTF-8');

// Include Composer autoloader if not already done.
include 'vendor/autoload.php';

//Funcao para captura dos dados via http
function crawl($url){

	$result = null;

	try{

		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		$result = curl_exec($ch);	

		curl_close($ch);

	}catch(Exception $e){
		echo "<br>Erro ao abrir o site principal: " . $e->getMessage();
	}

	return $result;
}

//Site principal
$site = crawl("http://transparencia.camarapoa.rs.gov.br/efetividade");

//Captura apenas links das efetividades disponiveis
preg_match_all("/<a +href=\"http:\/\/200.169.19.94\/documentos\/transparencia\/efetividade_vereadores\/.*/", $site, $matches). "\n";

//percorre links pegando nome dos arquivos e urls
foreach ($matches as $val) {
    foreach($val as $idx => $vlr){
    	$nome = preg_replace("/<a +href=\"http:\/\/200.169.19.94\/documentos\/transparencia\/efetividade_vereadores\//", '',$vlr);
    	$nome = preg_replace("/\"?>.*/", '',$nome);

    	$link = preg_replace("/<a href=\"/", '', $vlr);
    	$link = preg_replace("/\">.*/", '', $link);
    	
    	$links[$nome] = $link;

    	echo("Arquivo $links[$nome] carregado para o array.<br />");
    }
}

if(count($links) === 0){
	die('Nenhum link foi encontrado no site.');
}

$dirArqs = 'arq/';
try{
foreach ($links as $nome => $url) {
	$filename = $dirArqs.$nome;

	if (!file_exists($filename)) {
		//Cria novo arquivo temporario
		$arq = fopen($nome, 'w+');

	
		// Parse pdf file and build necessary objects.
		$parser = new \Smalot\PdfParser\Parser();
		$pdf    = $parser->parseFile($url);

		$txt .= utf8_encode($page->getText());

		fwrite($arq, $url);
	
		fclose($arq);

	} else {
		

	}	
}
}catch(Exception $e){
	echo "<br>Erro ao ler o arquivo $idx: " . $e->getMessage();
	continue;
}
die();
//Limpa arquivo temporario
$arq = 'temp_dados.txt';

if (!unlink($arq)) {
	echo ("Erro ao deletar $arq");
}
else {
	echo ("Deletado $arq com sucesso!");
}

//Cria novo arquivo temporario
$arq = fopen('temp_dados.txt', 'w');

//Percorre links salvos anteriormente
foreach ($links as $idx => $vlr) {

	try{
		// Parse pdf file and build necessary objects.
		$parser = new \Smalot\PdfParser\Parser();
		$pdf    = $parser->parseFile($vlr);
		$details = $pdf->getDetails();

	}catch(Exception $e){
		echo "<br>Erro ao ler o arquivo $idx: " . $e->getMessage();
		continue;
	}
	
	//seta encoding
	iconv_set_encoding("all", "Windows-1252");

	//Captura dados por pagina do arquivo atual
	$pages = $pdf->getPages();

	echo "<br>" . substr($details['ModDate'], 0,  10);

	$txt = '';
	$txt .= substr($details['ModDate'], 0, 10) . "\r\n";
	
	// Loop over each page to extract text.
	foreach ($pages as $idx => $page) {
		if($idx >= 3){
			$txt .= "\r\nPagina: " . $idx . "\r\n";
	    	$txt .= utf8_encode($page->getText());
	    	//$txt = preg_replace("/Pagina: [0-9](.*\n){1,10}Vereador/", '',$txt);
	    	//$txt = preg_replace("/\s+\nPagina(.*\n){1,5}\s*\n[0-9]{1,2}\s*\n/", '',$txt);
	    	//$txt = preg_replace("/CÃ¢mara Municipal de Porto Alegre, [0-9]{1,2} de [a-zA-Z]* de [0-9]{1,4}.?/", '',$txt);
	    	//$txt = preg_replace("/(\s\n){2,}/", "\r\n",$txt);
	    	//$txt = preg_replace("/Suplente(s)?:(.*\n){1,50}Sess.* Plen.*rio:\s?(\n\s)*/", "\r\nSessões: ",$txt);
	    	//$txt = preg_replace("/SECRETÃRIO\s*PRESIDENTE\s*/", "\r\n",$txt);
		}
	}
}

die("FIM");